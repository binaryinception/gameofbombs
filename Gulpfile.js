'use strict';
var gulp    = require('gulp'),
    uglify  = require('gulp-uglify'),
    ts      = require('gulp-typescript'),
    typings = require('gulp-typings');

gulp.task('typings', () => {
    return gulp
        .src('./typings.json')
        .pipe(typings());
})

gulp.task('script-dependencies', () => {
    return gulp
        .src('assets/js/*.js')
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('scripts', ['typings'], () => {
    let tsProject = ts.createProject('tsconfig.json', { watch: false });
    let tsResult = tsProject.src().pipe(tsProject());

    return tsResult.js.pipe(gulp.dest('./'));
});

gulp.task('compress', ['scripts'], () => {
    return gulp
        .src('dist/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('html', () => {
    return gulp
        .src('html/*.html')
        .pipe(gulp.dest('dist/'));
});



gulp.task('images', () => {
    return gulp
        .src('assets/images/*')
        .pipe(gulp.dest('dist/images/'));
});

gulp.task('maps', () => {
    return gulp
        .src('assets/maps/*.json')
        .pipe(gulp.dest('dist/maps/'));
});


gulp.task('default', ['images', 'html', 'maps', 'script-dependencies', 'scripts', 'compress'], () => {

});