module GOB {
    /**
     * The main Game object for Game Of Bombs
     */
    export class Game extends Phaser.Game {
        // Draw debug bodies
        public static debugPhysicsBodies:boolean = false;

        constructor() {
            super(1920, 1080, Phaser.AUTO, 'content', null);

            this.state.add('Boot', State.Boot, false);
            this.state.add('Game', State.Game, false);

            this.state.start('Boot');
        }
    }
}