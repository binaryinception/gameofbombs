module GOB.Input {
    /**
     * Debug input to forcefully move the camera with the cursor keys.
     */
    export class DebugCameraMovement implements InputInterface {
        public cursorKeys:Phaser.CursorKeys;
        public movementSpeed:number = 32;

        constructor(public game:GOB.Game) {
            this.cursorKeys = this.game.input.keyboard.createCursorKeys();
        }

        update() {
            let c = this.game.camera;

            if (this.cursorKeys.up.isDown) {
                c.y -= this.movementSpeed;
            }

            if (this.cursorKeys.down.isDown) {
                c.y += this.movementSpeed;
            }

            if (this.cursorKeys.left.isDown) {
                c.x -= this.movementSpeed;
            }

            if (this.cursorKeys.right.isDown) {
                c.x += this.movementSpeed;
            }
        }
    }
}