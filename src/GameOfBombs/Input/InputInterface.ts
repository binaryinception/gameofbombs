module GOB.Input {
    /**
     * Standard input handler interface
     */
    export interface InputInterface {
        game:Phaser.Game;

        update():void;
    }
}