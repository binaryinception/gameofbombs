module GOB.Input {
    /**
     * System to pickup specific objects with the mouse.
     */
    export class MousePickup implements InputInterface {
        public mouseSprite:Phaser.Sprite;
        public mouseBody:Phaser.Physics.P2.Body;
        public constraint:Phaser.Physics.P2.DistanceConstraint;
        public grabbedBody:Phaser.Physics.P2.Body;

        public onBeginGrab:Phaser.Signal = new Phaser.Signal();
        public onEndGrab:Phaser.Signal = new Phaser.Signal();

        constructor(public game:GOB.Game) {
            // Setup mouse pointer sprite and physics body
            this.mouseSprite = game.add.sprite(0, 0, 'star');
            this.mouseSprite.visible = false; // Hide it from the user

            // Create physics body for the mouse
            game.physics.p2.enable(this.mouseSprite, GOB.Game.debugPhysicsBodies);
            this.mouseBody = this.mouseSprite.body;
            this.mouseBody.setCircle(this.mouseSprite.width / 2);
            this.mouseBody.static = true;
            this.mouseBody.data.shapes[0].sensor = true;

            // Handle mouse input callbacks
            game.input.onDown.add(this.mouseClick, this);
            game.input.onUp.add(this.release, this); // on mouse up, release directly, no point to check it.
            game.input.addMoveCallback(this.mouseMove, this);
        }

        /**
         * Converts screen position to world position
         * @param pointer 
         */
        protected screenToWorldPoint(p:Phaser.Point) {
            return new Phaser.Point(
                this.game.camera.x + p.x,
                this.game.camera.y + p.y
            )
        }

        /**
         * Grab the give physics body and hold it with a constraint
         * @param body 
         */
        grab(body:Phaser.Physics.P2.Body) {
            this.release();

            this.constraint = this.game.physics.p2.createDistanceConstraint(this.mouseSprite, body, 10);
            this.grabbedBody = body;

            this.onBeginGrab.dispatch(body);
        }

        /**
         * Release the grabbed body!
         */
        release() {
            if (this.constraint && this.grabbedBody) {
                this.game.physics.p2.removeConstraint(this.constraint);
                this.onEndGrab.dispatch(this.grabbedBody);
                this.grabbedBody = null;
            }
        }

        /**
         * Mouse has clicked and is willing to pickup.
         * @param pointer 
         */
        protected mouseClick(pointer:Phaser.Pointer) {
            let objs = this.game.physics.p2.hitTest(this.screenToWorldPoint(pointer.position), undefined, 5, true);

            if (objs && objs.length) {
                let obj = objs[0];
                this.grab(obj);                
            }
        }

        /**
         * The mouse has moved, update the physics body of the mouse pointer.
         * @param pointer 
         * @param x 
         * @param y 
         * @param isDown 
         */
        protected mouseMove(pointer:Phaser.Pointer, x:number, y:number, isDown:boolean) {
            let worldPos = this.screenToWorldPoint(pointer.position);

            // Update the physics body to the mouse position.
            this.mouseSprite.body.x = worldPos.x; 
            this.mouseSprite.body.y = worldPos.y;
        }

        update () {
            // void.
        }
    }
}