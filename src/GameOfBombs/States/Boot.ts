module GOB.State {
    export class Boot extends Phaser.State {
        preload() {
            this.load.tilemap("tilemap", "maps/testmap.json", null, Phaser.Tilemap.TILED_JSON);
            this.load.image("tileset", "images/tiles_spritesheet.png");
            this.load.image("bomb", 'images/bomb.png');
            this.load.image("star", 'images/star.png');
            this.load.image("wooden_box", 'images/wooden_box.png');
            this.load.image("wooden_plank", 'images/wooden_plank.png');
        }

        create() {
            this.game.state.start("Game", true);
        }
    }
}