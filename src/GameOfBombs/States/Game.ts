module GOB.State {
    export class Game extends Phaser.State {
        // Map members
        public map:Phaser.Tilemap;
        public tileset:Phaser.Tileset;
        public groundLayer:Phaser.TilemapLayer;
        public waterLayer:Phaser.TilemapLayer;

        // Map objects
        public catapult:PhysicsObjects.Catapult;

        // Input members
        public inputs:Array<Input.InputInterface> = [];
        public mousePickup:Input.MousePickup;
        
        // Physics members
        public tileBodies:Array<Phaser.Physics.P2.Body>;
        public mapObjects:Array<PhysicsObjects.PhysicsObjectInterface> = [];

        /**
         * Initialize GameState
         * 
         * Load the tilemap and load the objects from the tilemap.
         */
        init() {
            this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
            this.scale.setUserScale(0.5, 0.5);
            
            // Setup map
            this.map = this.game.add.tilemap("tilemap");
            this.tileset = this.map.addTilesetImage("base", "tileset", 70, 70);
            this.waterLayer = this.map.createLayer("Water layer");            
            this.groundLayer = this.map.createLayer("Ground layer");
            this.map.setCollisionBetween(0, 9999, true, this.groundLayer); // All tiles in the ground layer are considered solid.
            this.groundLayer.resizeWorld();
        
            // Setup physics
            this.game.physics.startSystem(Phaser.Physics.P2JS);
            this.game.physics.p2.gravity.y = 1000;
            this.tileBodies = this.game.physics.p2.convertTilemap(this.map, this.groundLayer);
        }

        /**
         * Process individual object layers
         * @param objectLayer 
         */
        processObjectLayer(objectLayer:Array<any>) {
            objectLayer.forEach((obj) => this.processObject(obj));
        }

        /**
         * Process individual object from object layer
         * @param obj 
         */
        processObject(obj:any) {
            // We only handle objects of a certain type.
            if (obj.type != "PhysicsObject") {
                return;
            }

            // The future created map object.
            let newObj:PhysicsObjects.PhysicsObjectInterface;

            // Calculate the center position of the map object
            let center = new Phaser.Point(obj.x + (obj.width * 0.5), obj.y + (obj.height * 0.5));

            switch (obj.name) {
                case "Bomb": 
                    newObj = new PhysicsObjects.Bomb(this.game, center.x, center.y);
                    break;
                case "Catapult":
                    newObj = this.catapult = new PhysicsObjects.Catapult(this.game, center.x, center.y);
                    break;
                case "WoodenBox":
                    newObj = new PhysicsObjects.WoodenBox(this.game, center.x, center.y);
                    break;
                case "WoodenPlank":
                    newObj = new PhysicsObjects.WoodenPlank(this.game, center.x, center.y);
                    break;
                default:
                    console.warn('PhysicsObject not handled:', obj.type, obj.name);
                    return;
            }

            this.mapObjects.push(newObj);
        }
        
        create() {
            // Set the stage color
            this.game.stage.setBackgroundColor("#2d2d2d");

            // Setup input
            this.mousePickup = new Input.MousePickup(this.game);
            //this.inputs.push(new Input.DebugCameraMovement(this.game)); // Debug camera movement
            this.inputs.push(this.mousePickup);
            
            // Setup auto dispatch while being grabbed
            this.mousePickup.onBeginGrab.add(() => { this.catapult.disableAutoDetach = true }, this);
            this.mousePickup.onEndGrab.add(() => { this.catapult.disableAutoDetach = false }, this);

            // Process objects from the map
            for (let key in this.map.objects) {
                this.processObjectLayer(<Array<any>>this.map.objects[key]);
            }

            // Follow camera when fling.
            this.catapult.onDetach.add((body:PhysicsObjects.PhysicsObjectInterface) => { this.game.camera.follow(body, 0.1);  }, this);

            // Use the space for reloading
            this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR).onUp.add(() => this.reload(), this);

            // Use R for reload
            this.game.input.keyboard.addKey(Phaser.KeyCode.ENTER).onUp.add(() => this.game.state.start('Game'), this);
            
            // Reload!
            this.reload();
        }

        /**
         * Creates a new bomb and attaches it to the catapult.
         */
        reload() {
            let bomb = new PhysicsObjects.Bomb(this.game, this.catapult.x, this.catapult.y);
            this.catapult.disableAutoDetach = true;
            this.catapult.attach(bomb);

            // Double check if the camera is initialized the camera does not exist yet.
            if (this.game.camera) {
                this.game.camera.follow(this.catapult, 0.1, 0.1);
            }
        }

        /**
         * Update the game state.
         */
        update() {
            // Update registered input handlers.
            this.inputs.forEach((input) => input.update());
        }
    }
}