module GOB.PhysicsObjects {
    /**
     * The bomb that can be thrown
     */
    export class Bomb extends Phaser.Sprite implements PhysicsObjectInterface {
        constructor(game:Phaser.Game, x:number, y:number) {
            super(game, x, y, 'bomb');

            this.game.physics.p2.enable(this, GOB.Game.debugPhysicsBodies);
            
            let body:Phaser.Physics.P2.Body = this.body;

            body.setCircle(this.width / 2);
            body.damping = 0.5;
            body.mass = 2;
            
            game.add.existing(this);
        }
    }
}