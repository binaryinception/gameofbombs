module GOB.PhysicsObjects {
    /**
     * Simple wooden plank that is used for the levels.
     */
    export class WoodenPlank extends Phaser.Sprite implements PhysicsObjectInterface {
        constructor(game:Phaser.Game, x:number, y:number) {
            super(game, x, y, 'wooden_plank');

            this.game.physics.p2.enable(this, GOB.Game.debugPhysicsBodies);
            this.body.mass = 2;
            this.body.allowSleep = true;
            this.body.damping = 0.8;

            game.add.existing(this);
        }

    }
}