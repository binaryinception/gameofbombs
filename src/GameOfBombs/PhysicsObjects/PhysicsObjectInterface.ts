module GOB.PhysicsObjects {
    export interface PhysicsObjectInterface extends Phaser.Sprite {
        body:Phaser.Physics.P2.Body;
    }
}