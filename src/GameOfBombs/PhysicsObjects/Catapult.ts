module GOB.PhysicsObjects {
    /**
     * The catapult is a device that can connect to a physics body by a spring.
     * It can automatically release this body when instructed and collides with the catapult.
     * 
     * The catapult does not collide by itself.
     */
    export class Catapult extends Phaser.Sprite implements PhysicsObjectInterface {
        // The line for the spring
        private line:Phaser.Line;

        // Reference to the attached object
        private attachedObj:PhysicsObjectInterface;

        // The spring that attaches the body to the catapult
        private spring:Phaser.Physics.P2.Spring;

        // The catapult will not auto detached the body when this is set to true.
        // This is used when user input is involved so that the body is not accidentally detached.
        public disableAutoDetach:boolean = true;
        
        // Signal that triggers when a body is detached (automatic or by instruction).
        public onDetach:Phaser.Signal = new Phaser.Signal();

        constructor(game:Phaser.Game, x:number, y:number) {
            super(game, x, y, 'star');

            // Setup body for the catapult
            this.game.physics.p2.enable(this, GOB.Game.debugPhysicsBodies);
            this.body.data.shapes[0].sensor = true;
            this.body.static = true;

            // Reference for collision with objects
            this.body.onBeginContact.add(this.onBeginContact, this);
            
            // Add the catapult to the game.
            game.add.existing(this);

            // Create the line that draws the spring
            this.line = new Phaser.Line();
        }

        /**
         * Attach object to the catapult through a spring
         * @param obj 
         */
        attach(obj:PhysicsObjectInterface) {
            this.attachedObj = obj;
            this.spring = this.game.physics.p2.createSpring(this, obj, 1, 100, 1);
        }

        /**
         * Update, render a line between grabbed body and catapult to display the spring
         */
        update() {
            if (!this.attachedObj) {
                return;
            }
            
            // Draw debug line to the attachee
            this.line.setTo(this.x, this.y, this.attachedObj.x, this.attachedObj.y);
            this.game.debug.geom(this.line);
        }

        /**
         * Something hit us. Is it the attachee? Release it!
         * @param otherBody 
         */
        onBeginContact(otherBody:Phaser.Physics.P2.Body) {
            if (!this.attachedObj || this.disableAutoDetach) {
                return;
            }
            
            // Check if it is the attached object
            if (this.attachedObj.body == otherBody) {
                this.detach();
            }
        }

        /**
         * Detach the attached obj.
         */
        detach() {
            if (this.spring && this.attachedObj) {
                this.game.physics.p2.removeSpring(this.spring);
                this.onDetach.dispatch(this.attachedObj);
                
                // clean up.
                this.spring = null;
                this.attachedObj = null;
            }
        }
    }
}