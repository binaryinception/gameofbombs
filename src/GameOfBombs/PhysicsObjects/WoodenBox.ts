module GOB.PhysicsObjects {
    /**
     * Simple wooden box that is used for the levels.
     */
    export class WoodenBox extends Phaser.Sprite implements PhysicsObjectInterface {
        constructor(game:Phaser.Game, x:number, y:number) {
            super(game, x, y, 'wooden_box');

            this.game.physics.p2.enable(this, GOB.Game.debugPhysicsBodies);
            this.body.mass = 3;
            this.body.allowSleep = true;
            this.body.damping = 0.9;

            game.add.existing(this);
        }

    }
}