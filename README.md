# Requirements
- Nodejs

# Setup your project
1. Clone this repository
2. Navigate to the project folder
3. Install dependencies: `npm install`
4. Run gulp: `gulp` or `node_modules/.bin/gulp`

# How to test your project

## Custom web host
Make sure the `dist` folder is accessible on any webserver and access that webserver.

## Using simple-server.js

### Requirements
- Install dev dependencies `npm install --dev`

### Run!
Just start `node simple-server.js` and your game is available on the given port. You can change the port by change the `PORT` environment variable. For example: `PORT=1234 node simple-server.js`.

